### Symptom Search API
Currently in BETA testing

#### url

POST /apiV1/symptom_search/

#### Body Parameters

```
{
  "symptoms":["coughing","shortness of breath","fluid in the lungs"],
  "animal_type":"cats",
  "breed":"siamese",
  "date_of_birth":"2010-07-31"

}
```

Symptoms search is a JSON list of symptoms.

Currently Animal type must be one of the following: (a bespoke dictionary can be implemented if necessary for the customer)
```
ANIMAL_TYPE_CHOICES_SEARCH= [

        ('birds'),
        ('cattle'),
        ('cats'),
        ('chickens'),
        ('dogs'),
        ('exotic'),
        ('goats'),
        ('horses'),
        ('laboratory'),
        ('pigs'),
        ('sheep'),

        ]
```

Breed: Optional
Date of Birth: Optional


#### Example Output


```
HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "items": [
        {
            "disease_tags": [
                "Trichobezoar",
                "Vomiting"
            ],
            "author": "WEBB.CRAIG",
            "title": "Vomiting  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "48c1e012c1f950368cce3b3386b4d963",
            "prevalence_score": 29.0,
            "url": "https://wise.vet/content/view_content/48c1e012c1f950368cce3b3386b4d963/"
        },
        {
            "disease_tags": [
                "Abscess"
            ],
            "author": "RIBEIRO.MARCIO",
            "title": "Nocardiosis  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "2674b14a4dfc552cb291cd0a97ba3cb0",
            "prevalence_score": 32.1,
            "url": "https://wise.vet/content/view_content/2674b14a4dfc552cb291cd0a97ba3cb0/"
        },
        {
            "disease_tags": [
                "Obesity"
            ],
            "author": "KUTZLER.MICHELLE",
            "title": "Mammary Tumors in Dogs and Cats - Reproductive System - Veterinary Manual",
            "uuid": "2eb6bf7257865035895f6e8414cab373",
            "prevalence_score": 66.7,
            "url": "https://wise.vet/content/view_content/2eb6bf7257865035895f6e8414cab373/"
        },
        {
            "disease_tags": [
                "Weight loss",
                "Gastritis",
                "Diarrhoea finding",
                "Vomiting"
            ],
            "author": "PEREGRINE.ANDREW",
            "title": "Gastrointestinal Parasites  of Cats - Cat Owners - Veterinary Manual",
            "uuid": "a1a0a2ee878d5d8fbbba99ecef037a9d",
            "prevalence_score": 29.0,
            "url": "https://wise.vet/content/view_content/a1a0a2ee878d5d8fbbba99ecef037a9d/"
        },
        {
            "disease_tags": [
                "Appetite decreased",
                "Lethargy",
                "Hyperthyroidism"
            ],
            "author": "PETERSON.MARK;KRITCHEVSKY.JANICE",
            "title": "Disorders of the Thyroid Gland  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "e8baa95e6adc55079c2d1ce31ea21097",
            "prevalence_score": 29.6,
            "url": "https://wise.vet/content/view_content/e8baa95e6adc55079c2d1ce31ea21097/"
        },
        {
            "disease_tags": [
                "Rhinitis",
                "Sneezing",
                "Conjunctivitis"
            ],
            "author": "KUEHN.NED",
            "title": "Feline Respiratory Disease Complex - Respiratory System - Veterinary Manual",
            "uuid": "ef2846c93ccb5d759eea6c019855acf7",
            "prevalence_score": 30.1,
            "url": "https://wise.vet/content/view_content/ef2846c93ccb5d759eea6c019855acf7/"
        },
        {
            "disease_tags": [
                "Nail clip"
            ],
            "author": "BUKOWSKI.JOHN",
            "title": "Description and Physical Characteristics  of Cats - Cat Owners - Veterinary Manual",
            "uuid": "799c85e5f7d659708c8b365df2c5bc40",
            "prevalence_score": 36.8,
            "url": "https://wise.vet/content/view_content/799c85e5f7d659708c8b365df2c5bc40/"
        },
        {
            "disease_tags": [
                "Appetite decreased",
                "Weight loss",
                "Vomiting",
                "Abscess"
            ],
            "author": "STEINER.JORG",
            "title": "Disorders of the Pancreas  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "b84fe7e4ead05f63a9d640962f5612e3",
            "prevalence_score": 32.1,
            "url": "https://wise.vet/content/view_content/b84fe7e4ead05f63a9d640962f5612e3/"
        },
        {
            "disease_tags": [
                "Wound"
            ],
            "author": "REITER.ALEXANDER",
            "title": "Disorders of the Mouth  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "13c549b1f62b57ff8f6b32056b81c383",
            "prevalence_score": 22.9,
            "url": "https://wise.vet/content/view_content/13c549b1f62b57ff8f6b32056b81c383/"
        },
        {
            "disease_tags": [
                "Seizure disorder",
                "Hyperthyroidism"
            ],
            "author": "RANDOLPH.JOHN",
            "title": "Erythrocytosis (Polycythemia)  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "a222eff0670d54c282f413df3b0062f7",
            "prevalence_score": 29.6,
            "url": "https://wise.vet/content/view_content/a222eff0670d54c282f413df3b0062f7/"
        }
    ]
}HTTP 200 OK
Allow: POST, OPTIONS
Content-Type: application/json
Vary: Accept

{
    "items": [
        {
            "disease_tags": [
                "Trichobezoar",
                "Vomiting"
            ],
            "author": "WEBB.CRAIG",
            "title": "Vomiting  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "48c1e012c1f950368cce3b3386b4d963",
            "prevalence_score": 29.0,
            "url": "https://wise.vet/content/view_content/48c1e012c1f950368cce3b3386b4d963/"
        },
        {
            "disease_tags": [
                "Abscess"
            ],
            "author": "RIBEIRO.MARCIO",
            "title": "Nocardiosis  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "2674b14a4dfc552cb291cd0a97ba3cb0",
            "prevalence_score": 32.1,
            "url": "https://wise.vet/content/view_content/2674b14a4dfc552cb291cd0a97ba3cb0/"
        },
        {
            "disease_tags": [
                "Obesity"
            ],
            "author": "KUTZLER.MICHELLE",
            "title": "Mammary Tumors in Dogs and Cats - Reproductive System - Veterinary Manual",
            "uuid": "2eb6bf7257865035895f6e8414cab373",
            "prevalence_score": 66.7,
            "url": "https://wise.vet/content/view_content/2eb6bf7257865035895f6e8414cab373/"
        },
        {
            "disease_tags": [
                "Weight loss",
                "Gastritis",
                "Diarrhoea finding",
                "Vomiting"
            ],
            "author": "PEREGRINE.ANDREW",
            "title": "Gastrointestinal Parasites  of Cats - Cat Owners - Veterinary Manual",
            "uuid": "a1a0a2ee878d5d8fbbba99ecef037a9d",
            "prevalence_score": 29.0,
            "url": "https://wise.vet/content/view_content/a1a0a2ee878d5d8fbbba99ecef037a9d/"
        },
        {
            "disease_tags": [
                "Appetite decreased",
                "Lethargy",
                "Hyperthyroidism"
            ],
            "author": "PETERSON.MARK;KRITCHEVSKY.JANICE",
            "title": "Disorders of the Thyroid Gland  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "e8baa95e6adc55079c2d1ce31ea21097",
            "prevalence_score": 29.6,
            "url": "https://wise.vet/content/view_content/e8baa95e6adc55079c2d1ce31ea21097/"
        },
        {
            "disease_tags": [
                "Rhinitis",
                "Sneezing",
                "Conjunctivitis"
            ],
            "author": "KUEHN.NED",
            "title": "Feline Respiratory Disease Complex - Respiratory System - Veterinary Manual",
            "uuid": "ef2846c93ccb5d759eea6c019855acf7",
            "prevalence_score": 30.1,
            "url": "https://wise.vet/content/view_content/ef2846c93ccb5d759eea6c019855acf7/"
        },
        {
            "disease_tags": [
                "Nail clip"
            ],
            "author": "BUKOWSKI.JOHN",
            "title": "Description and Physical Characteristics  of Cats - Cat Owners - Veterinary Manual",
            "uuid": "799c85e5f7d659708c8b365df2c5bc40",
            "prevalence_score": 36.8,
            "url": "https://wise.vet/content/view_content/799c85e5f7d659708c8b365df2c5bc40/"
        },
        {
            "disease_tags": [
                "Appetite decreased",
                "Weight loss",
                "Vomiting",
                "Abscess"
            ],
            "author": "STEINER.JORG",
            "title": "Disorders of the Pancreas  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "b84fe7e4ead05f63a9d640962f5612e3",
            "prevalence_score": 32.1,
            "url": "https://wise.vet/content/view_content/b84fe7e4ead05f63a9d640962f5612e3/"
        },
        {
            "disease_tags": [
                "Wound"
            ],
            "author": "REITER.ALEXANDER",
            "title": "Disorders of the Mouth  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "13c549b1f62b57ff8f6b32056b81c383",
            "prevalence_score": 22.9,
            "url": "https://wise.vet/content/view_content/13c549b1f62b57ff8f6b32056b81c383/"
        },
        {
            "disease_tags": [
                "Seizure disorder",
                "Hyperthyroidism"
            ],
            "author": "RANDOLPH.JOHN",
            "title": "Erythrocytosis (Polycythemia)  in Cats - Cat Owners - Veterinary Manual",
            "uuid": "a222eff0670d54c282f413df3b0062f7",
            "prevalence_score": 29.6,
            "url": "https://wise.vet/content/view_content/a222eff0670d54c282f413df3b0062f7/"
        }
    ]
}
```